#!/bin/bash
#SBATCH --job-name=test
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-user=

module load nextflow
module load singularity


nextflow run main.nf -profile singularity,xanadu -params-file params.yaml 


#!/usr/bin/env python

########################################################
#                                                      #
# random_forest_classifier_predict.py - TIS prediction #
# with random forest classifier and feature matrix     #
#                                                      #
# Developed by Cynthia Webster (2022)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################

from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import pandas as pd
import numpy as np
from sklearn.metrics import f1_score,mean_squared_error, accuracy_score
from sklearn.metrics import r2_score,mean_squared_error
from sklearn.feature_selection import RFECV
import matplotlib.pyplot as plt
import sys
import re

def clean_dataset(df):
    assert isinstance(df, pd.DataFrame), "df needs to be a pd.DataFrame"
    df.dropna(inplace=True)
    indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(1)
    return df[indices_to_keep]


def get_feature_importances(cols, importances):
    feats = {}
    for feature, importance in zip(cols, importances):
        feats[feature] = importance

    importances = pd.DataFrame.from_dict(feats, orient='index').rename(columns={0: 'Gini-importance'})

    return importances.sort_values(by='Gini-importance', ascending = False)

def compare_values(arr1, arr2):
    thediff = 0
    thediffs = []
    for thing1, thing2 in zip(arr1, arr2):
        thediff = abs(thing1 - thing2)
        thediffs.append(thediff)

    return thediffs

def print_to_file(filepath, arr):
    with open(filepath, 'w') as f:
        for item in arr:
            f.write("%s\n" % item)

training_set=sys.argv[1]
testing_set=sys.argv[2]
output=sys.argv[3]

raw_data=pd.read_csv(training_set, delimiter="\t")
data=clean_dataset(raw_data)

# create the labels, or field we are trying to estimate
label = data['Target']

# drop F1 and Target
data = data.drop('F1', axis=1)
data = data.drop('Target', axis=1)


#split into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(data, label, test_size = 0.2)
rf = RandomForestClassifier(n_estimators = 100)
rf.fit(X_train, y_train)

# Evaluating on Training set
rfc_pred_train = rf.predict(X_train)
print(rfc_pred_train)
print('Training Set Evaluation RMSE=>',np.sqrt(mean_squared_error(y_train,rfc_pred_train)))
print('Training Set Evaluation r2=>',r2_score(y_train,rfc_pred_train))

# Evaluating on Test set
rfc_pred_test = rf.predict(X_test)
print('Testing Set Evaluation RMSE=>',np.sqrt(mean_squared_error(y_test,rfc_pred_test)))
print('Testing Set Evaluation r2=>',r2_score(y_test,rfc_pred_test))
print('Testing Set Evaluation accuracy =>', accuracy_score(y_test, rfc_pred_test))

rf_all=RandomForestClassifier(n_estimators = 100)
rf_all.fit(data, label)

new_raw_data=pd.read_csv(testing_set, delimiter=r"\s+")
new_data=clean_dataset(new_raw_data)

new_order_numbers = new_data['Transcript']
new_data=new_data.drop(['Gene','Transcript'], axis = 1)
print(list(new_data.columns))
rf_pred = rf_all.predict(new_data)

res= pd.DataFrame({'Transcript': new_order_numbers, 'Prediction': rf_pred})
res.to_csv(output, index=False)
#!/bin/bash

echo "Searching for files..."
# File extension to search for
mikado=$(find . -type f -path './mikado/*.stats')
echo $mikado
busco=$(find . -type f -path './busco/*/short_summary.specific*.txt')
echo $busco
diamond=$(find . -type f -path './diamond/*final.txt')
echo $diamond
agat=$(find . -type f -path './agat/*.txt')
echo $agat

echo "Processing BUSCO results..."
# BUSCO
# Extract specific values using awk
percentages_line=$(grep "C:" $busco)

# Extract specific percentages using awk
c_percentage=$(echo "$percentages_line" | grep -oE 'C:[0-9.]+%' | cut -d':' -f2)
s_percentage=$(echo "$percentages_line" | grep -oE 'S:[0-9.]+%' | cut -d':' -f2)
d_percentage=$(echo "$percentages_line" | grep -oE 'D:[0-9.]+%' | cut -d':' -f2)
f_percentage=$(echo "$percentages_line" | grep -oE 'F:[0-9.]+%' | cut -d':' -f2)
m_percentage=$(echo "$percentages_line" | grep -oE 'M:[0-9.]+%' | cut -d':' -f2)

# Print the formatted information to metrics.txt
echo -e "Complete BUSCOs (C)\t$c_percentage" > metrics.txt
echo -e "Complete and single-copy BUSCOs (S)\t$s_percentage" >> metrics.txt
echo -e "Complete and duplicated BUSCOs (D)\t$d_percentage" >> metrics.txt
echo -e "Fragmented BUSCOs (F)\t$f_percentage" >> metrics.txt
echo -e "Missing BUSCOs (M)\t$m_percentage" >> metrics.txt

echo "Processing AGAT results..."
# AGAT

gene_num=$(grep 'Number of gene' "$agat" | head -n 1 | grep -o '[0-9]\+')
transcript_num=$(grep 'Number of transcript' "$agat" | head -n 1 | grep -o '[0-9]\+')
single_exon=$(grep 'Number of single exon gene' "$agat" | head -n 1 | grep -o '[0-9]\+')
mean_exon=$(grep 'mean exons per transcript' "$agat" | head -n 1 | grep -o '[0-9]\+\.*[0-9]*')
gene_length=$(grep 'mean gene length' "$agat" | head -n 1 | grep -o '[0-9]\+' )
transcript_length=$(grep 'mean transcript length' "$agat" | head -n 1 | grep -o '[0-9]\+')

multi=$((gene_num - single_exon))
mono_multi=$(echo "scale=2 ; $single_exon / $multi" | bc) 
echo " " >> metrics.txt
echo -e "Total number of genes\t$gene_num" >> metrics.txt
echo -e "Total number of transcripts\t$transcript_num" >> metrics.txt
echo -e "Total number of single exon genes\t$single_exon" >> metrics.txt
echo -e "mono:multi ratio\t$mono_multi" >> metrics.txt
echo -e "Mean exons per transcript\t$mean_exon" >> metrics.txt
echo -e "Mean gene length\t$gene_length" >> metrics.txt
echo -e "Mean transcript length\t$transcript_length" >> metrics.txt

echo "Processing DIAMOND results..."
# DIAMOND
diamond_align=$(cat $diamond)
echo "" >> metrics.txt
echo -e "Number of BLAST alignments\t$diamond_align" >> metrics.txt
echo "" >> metrics.txt
echo "Processing MIKADO results..."
# MIKADO
if [[ -n "$mikado" ]]; then
    # Extract specific lines using grep
    transcript_line=$(grep 'Transcript level (stringent)' "$mikado")
    gene_100_f1_line=$(grep 'Gene level (100% base F1)' "$mikado")

# Extract sensitivity, precision, and F1 values using awk
transcript_values=$(echo "$transcript_line" | awk '{print $4 "\t" $5 "\t" $6 }')
gene_100_f1_values=$(echo "$gene_100_f1_line" | awk '{print $6 "\t" $7 "\t" $8}')

# Print the formatted information to metrics.txt
echo -e "\t                       \tSn\tPr\tF1" >> metrics.txt
echo -e "Transcript level (stringent)\t$transcript_values" >> metrics.txt
echo -e "Gene level (100% base F1)\t$gene_100_f1_values" >> metrics.txt
fi

echo "Script finished!"

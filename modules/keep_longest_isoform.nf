process gff_isoform {
    label 'process_medium'
    publishDir "$params.outdir",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    val(prefix)

    output: 
    path("*_filtered_longest_isoform.gff"), emit: filtered_gff
    path("*_filtered_longest_isoform.gtf"), emit: filtered_gtf
      
    """

    agat_sp_keep_longest_isoform.pl --gff ${gff} -o ${prefix}_filtered_longest_isoform.gff
    agat_convert_sp_gff2gtf.pl --gff ${prefix}_filtered_longest_isoform.gff -o ${prefix}_filtered_longest_isoform.gtf

    """
}

process nucleotide_isoform {
    publishDir "$params.outdir",  mode: 'copy', pattern: "*.cds"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(genome)
    path(filtered_prediction)
    val(prefix)
     
    output: 
    path("*.cds"), emit: cds
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} -t cds -o ${prefix}_filtered_longest_isoform.cds

    """
}
process protein_isoform {
    publishDir "$params.outdir",  mode: 'copy', pattern: "*.pep"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(filtered_prediction)
    path(genome)
    val(prefix)
     
    output: 
    path("*.pep"), emit: protein
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} --protein -o ${prefix}_filtered_longest_isoform.pep

    """
}
process busco_isoform {
 label 'process_medium'
    publishDir "$params.outdir/metrics/busco_longest_isoform",  mode: 'copy' 

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.6--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.6--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)
    val(prefix)

    output:
    path("$prefix*/*"), emit: busco_unfiltered
    path("$prefix*/*.txt"), emit: busco_unfiltered_txt
      
    """
    busco -i ${protein} -l ${odb} -o ${prefix} -m Protein -c ${task.cpus}

    """
}
process agat_isoform {
 label 'process_medium'
    publishDir "$params.outdir/metrics/agat_longest_isoform",  mode: 'copy' 

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.txt"), emit: unfiltered_stats
      
    """
    agat_sp_statistics.pl --gff ${prediction} -o ${prefix}.txt
    """
}
process diamond_isoform {
 label 'process_medium'
    publishDir "$params.outdir/metrics/diamond_longest_isoform",  mode: 'copy' 
    
    conda "bioconda::diamond"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.0.6--h56fc30b_0' :
        'quay.io/biocontainers/diamond:2.0.6--h56fc30b_0' }"
   
    input:
    path(protein)
    path(database)
    val(id)
 
    output:
    path("*final.txt"), emit: stats
    path("${id}.txt"), emit: annot
      
    """
   diamond blastp --more-sensitive --query-cover 70 --subject-cover 70 -d ${database} -q ${protein} -k 1 -o ${id}.txt --threads ${task.cpus}
    count1=\$(wc -l < ${id}.txt)
    echo \$count1 > ${id}_final.txt
    """
}

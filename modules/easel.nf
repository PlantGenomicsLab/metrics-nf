process regressor {
    label 'process_low'
    publishDir "$params.outdir/easel_filtering",  mode: 'copy' 
    container 'cynthiawebster/easel:python'
    
    input:
    path(test)
    path(train)
     
    output: 
    path("regressor_prediction.csv"), emit: r_rf
      
    """
    python ${projectDir}/bin/random_forest_regressor_predict.py ${train} ${test} regressor.csv
    python ${projectDir}/bin/random_forest_regressor_predict_na.py ${train} ${test} na.csv
    cat regressor.csv na.csv > regressor_prediction.csv

    """
}
process classifier {
    label 'process_low'
    publishDir "$params.outdir/easel_filtering",  mode: 'copy' 
    container 'cynthiawebster/easel:python'
    
    input:
    path(test)
    path(train)
     
    output: 
    path("classifier_prediction.csv"), emit: c_rf
      
    """
    python ${projectDir}/bin/random_forest_classifier_predict.py ${train} ${test} classifier_prediction.csv

    """
}

process filtered_gtf {
    label 'process_low'
    publishDir "$params.outdir",  mode: 'copy' 

    input:
    path(classifier)
    path(regressor)
    path(unfiltered_gtf)
    val(prefix)
    val(reg_val)
     
    output: 
    path("*filtered.gtf"), emit: filtered_prediction
      
    """
 
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 
tail -n +2 ${regressor} | awk -F"," '\$2>${reg_val}' >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /transcript_id "([^"]+)";/, m); if (m[1] in transcripts) print }' transcripts.txt ${unfiltered_gtf} > ${prefix}_filtered.gtf

    """
}
process filtered_gff {
    label 'process_low'
    publishDir "$params.outdir",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gtf)
    val(prefix)

    output: 
    path("*filtered.gff"), emit: filtered_gff
      
    """

agat_convert_sp_gxf2gxf.pl --gff ${gtf} --out ${prefix}_filtered.gff

    """
}
process filtered_protein {
    publishDir "$params.outdir",  mode: 'copy', pattern: "*.pep"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(filtered_prediction)
    path(genome)
    val(prefix)
     
    output: 
    path("*_filtered.pep"), emit: protein
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} --protein -o ${prefix}_filtered.pep 

    """
}
process filtered_nucleotide {
    publishDir "$params.outdir",  mode: 'copy', pattern: "*.cds"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(genome)
    path(filtered_prediction)
    val(prefix)
     
    output: 
    path("*_filtered.cds"), emit: cds
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} -t cds -o ${prefix}_filtered.cds 

    """
}
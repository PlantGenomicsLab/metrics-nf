process gtf {
    publishDir "$params.outdir",  mode: 'copy'
    label 'process_low'
    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1 ' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1 ' }"

    input:
    path(prediction)
    val(prefix)
     
    output: 
    path("*.gtf"), emit: gtf
      
    """
    gffread -E ${prediction} -T -o ${prefix}.gtf
    """   
}
process mikado {
   label 'process_medium'
    publishDir "$params.outdir/metrics/mikado",  mode: 'copy'
    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(prediction)
    path(reference)
    val(id)
     
    output: 
    path("$id*"), emit: prediction
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}
    """
}
process protein {
 label 'process_medium'
 publishDir "$params.outdir",  mode: 'copy'
    input:
    path(genome)
    path(unfiltered_prediction)
    val(id)

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
     
    output: 
    path("*.pep"), emit: protein

    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} --protein -o "$id".pep 
    """
}
process busco {
 label 'process_medium'
    publishDir "$params.outdir/metrics/busco",  mode: 'copy' 

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.6--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.6--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)
    val(prefix)

    output:
    path("$prefix*/*"), emit: busco_unfiltered
    path("$prefix*/*.txt"), emit: busco_unfiltered_txt
      
    """
    busco -i ${protein} -l ${odb} -o ${prefix} -m Protein -c ${task.cpus}

    """
}
process agat {
 label 'process_medium'
    publishDir "$params.outdir/metrics/agat",  mode: 'copy' 

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.txt"), emit: unfiltered_stats
      
    """
    agat_sp_statistics.pl --gff ${prediction} -o ${prefix}.txt
    """
}
process diamond {
 label 'process_medium'
    publishDir "$params.outdir/metrics/diamond",  mode: 'copy' 
    
    conda "bioconda::diamond"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.0.6--h56fc30b_0' :
        'quay.io/biocontainers/diamond:2.0.6--h56fc30b_0' }"
   
    input:
    path(protein)
    path(database)
    val(id)
 
    output:
    path("*final.txt"), emit: stats
    path("${id}.txt"), emit: annot
      
    """
   diamond blastp --more-sensitive --query-cover 70 --subject-cover 70 -d ${database} -q ${protein} -k 1 -o ${id}.txt --threads ${task.cpus}
    count1=\$(wc -l < ${id}.txt)
    echo \$count1 > ${id}_final.txt
    """
}

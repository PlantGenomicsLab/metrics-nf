# Metrics Nextflow Pipeline

[![Nextflow](https://img.shields.io/badge/nextflow%20DSL2-%E2%89%A522.10.1-23aa62.svg)](https://www.nextflow.io/)
[![run with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg?labelColor=000000)](https://sylabs.io/docs/)

Metrics
>- BUSCO
>- AGAT
>- DIAMOND
>- Mikado 

Required Dependencies:
>- Nextflow
>- Singularity

#### Provide desired inputs in a yaml file
   ```
   module load nextflow
   module load singularity

   SINGULARITY_TMPDIR=/core/labs/Wegrzyn/easel/tmp
   export SINGULARITY_TMPDIR

   nextflow run -hub gitlab PlantGenomicsLab/metrics-nf -profile singularity,xanadu \
       -params-file params.yaml
   ```
**params.yaml:**
```
genome : "/core/labs/Wegrzyn/easel/comp_genomics/data/arabidopsis.fasta"
busco_lineage : "embryophyta"
prefix : "arabidopsis"
refseq_db : "/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd"
reference : "/core/labs/Wegrzyn/easel/comp_genomics/data/arabidopsis.gtf"
gff : "/core/labs/Wegrzyn/easel/comp_genomics/helixer/arabidopsis/Arabidopsis.gff3"
singularity_cache_dir: "/isg/shared/databases/nfx_singularity_cache"
```
**Notes:**
>- Copy and paste a relevant `busco_lineage` from the following [`file`](https://gitlab.com/PlantGenomicsLab/easel/-/blob/main/bin/busco_lineage.txt), capital letters matter!
>- `refseq_db` will be either complete or plant refseq DIAMOND database
>- REFERENCE GTF may cause Mikado to fail, I suggest running it quickly through gffread to re-format it:
   `gffread my.gff3 -T -o my.gtf`
>- If you're working with a non-model just put a dummy GTF file in the reference parameter


#### Run EASEL Training
Because we are constantly updating the training sets I have provided a way to quickly filter and generate a new gff/gtf/pep/cds file - as well as run metrics scripts!

**params.yaml:**
```
genome : "/core/labs/Wegrzyn/easel/comp_genomics/data/arabidopsis.fasta"
busco_lineage : "embryophyta"
prefix : "arabidopsis"
refseq_db : "/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd"
reference : "/core/labs/Wegrzyn/easel/comp_genomics/data/arabidopsis.gtf"
gff : "/core/labs/Wegrzyn/easel/comp_genomics/helixer/arabidopsis/Arabidopsis.gff3"
singularity_cache_dir: "/isg/shared/databases/nfx_singularity_cache"
features : "/core/labs/Wegrzyn/easel/benchmarking/easel/v_1.3/plant/arabidopsis/arabidopsis/07_filtering/features.tracking"
gff : "/core/labs/Wegrzyn/easel/benchmarking/easel/v_1.3/plant/arabidopsis/arabidopsis/final_predictions/arabidopsis_unfiltered.gff"
gtf : "/core/labs/Wegrzyn/easel/benchmarking/easel/v_1.3/plant/arabidopsis/arabidopsis/final_predictions/arabidopsis_unfiltered.gtf"
training_set : "plant"
```
**Notes:**
>- Training set options include `plant`, `invertebrate`, and `vertebrate`



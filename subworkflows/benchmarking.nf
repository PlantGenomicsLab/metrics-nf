include { percent_masked; fold_genome } from '../modules/split_genome.nf'
include { gtf; mikado; protein; busco; agat; diamond } from '../modules/metrics.nf'
include { regressor; classifier; filtered_gtf; filtered_gff; filtered_protein; filtered_nucleotide } from '../modules/easel.nf'
include { gff_isoform; nucleotide_isoform; protein_isoform; agat_isoform; diamond_isoform; busco_isoform } from '../modules/keep_longest_isoform.nf'


workflow data_input {

    main:
    percent_masked          ( params.genome )
    fold_genome             ( params.genome, params.prefix )

    if(params.training_set == "plant"){
    regressor               ( params.features, params.plant )
    classifier              ( params.features, params.plant )
    filtered_gtf            ( classifier.out.c_rf, regressor.out.r_rf, params.gtf, params.prefix, params.regressor )
    filtered_gff            ( filtered_gtf.out.filtered_prediction, params.prefix )
    filtered_protein        ( filtered_gtf.out.filtered_prediction, fold_genome.out.fold, params.prefix )
    filtered_nucleotide     ( fold_genome.out.fold, filtered_gtf.out.filtered_prediction, params.prefix )
    busco                   ( filtered_protein.out.protein, params.busco_lineage, params.prefix )
    agat                    ( filtered_gtf.out.filtered_prediction, params.prefix )
    diamond                 ( filtered_protein.out.protein, params.refseq_db, params.prefix ) 
    mikado                  ( filtered_gtf.out.filtered_prediction, params.reference, params.prefix )
    }

    else if(params.training_set == "vertebrate"){
    regressor               ( params.features, params.vertebrate )
    classifier              ( params.features, params.vertebrate )
    filtered_gtf            ( classifier.out.c_rf, regressor.out.r_rf, params.gtf, params.prefix, params.regressor )
    filtered_gff            ( filtered_gtf.out.filtered_prediction, params.prefix )
    filtered_protein        ( filtered_gtf.out.filtered_prediction, fold_genome.out.fold, params.prefix )
    filtered_nucleotide     ( fold_genome.out.fold, filtered_gtf.out.filtered_prediction, params.prefix )
    busco                   ( filtered_protein.out.protein, params.busco_lineage, params.prefix )
    agat                    ( filtered_gtf.out.filtered_prediction, params.prefix )
    diamond                 ( filtered_protein.out.protein, params.refseq_db, params.prefix ) 
    mikado                  ( filtered_gtf.out.filtered_prediction, params.reference, params.prefix )

    }

    else if(params.training_set == "invertebrate"){
    regressor               ( params.features, params.invertebrate )
    classifier              ( params.features, params.invertebrate )
    filtered_gtf            ( classifier.out.c_rf, regressor.out.r_rf, params.gtf, params.prefix, params.regressor )
    filtered_gff            ( filtered_gtf.out.filtered_prediction, params.prefix )
    filtered_protein        ( filtered_gtf.out.filtered_prediction, fold_genome.out.fold, params.prefix )
    filtered_nucleotide     ( fold_genome.out.fold, filtered_gtf.out.filtered_prediction, params.prefix )
    busco                   ( filtered_protein.out.protein, params.busco_lineage, params.prefix )
    agat                    ( filtered_gtf.out.filtered_prediction, params.prefix )
    diamond                 ( filtered_protein.out.protein, params.refseq_db, params.prefix ) 
    mikado              ( filtered_gtf.out.filtered_prediction, params.reference, params.prefix )
    }

    else{
    gtf                     ( params.gff, params.prefix )
    protein                 ( fold_genome.out.fold, gtf.out.gtf, params.prefix )
    busco                   ( protein.out.protein, params.busco_lineage, params.prefix )
    agat                    ( gtf.out.gtf, params.prefix )
    diamond                 ( protein.out.protein, params.refseq_db, params.prefix ) 
    mikado              ( gtf.out.gtf, params.reference, params.prefix )
    }
    
    if(params.longest_isoform == true && params.training_set == null ){
    gff_isoform         ( params.gff, params.prefix )
    nucleotide_isoform  ( fold_genome.out.fold, gff_isoform.out.filtered_gff, params.prefix )
    protein_isoform     ( gff_isoform.out.filtered_gff, fold_genome.out.fold, params.prefix )
    agat_isoform        ( gff_isoform.out.filtered_gtf, params.prefix )
    diamond_isoform     ( protein_isoform.out.protein, params.refseq_db, params.prefix )
    busco_isoform       ( protein_isoform.out.protein, params.busco_lineage, params.prefix )
    }
    else if(params.longest_isoform == true && params.training_set == "plant" || params.training_set == "invertebrate" || params.training_set == "vertebrate" ){
    gff_isoform         ( filtered_gtf.out.filtered_prediction, params.prefix )
    nucleotide_isoform  ( fold_genome.out.fold, gff_isoform.out.filtered_gff, params.prefix )
    protein_isoform     ( gff_isoform.out.filtered_gff, fold_genome.out.fold, params.prefix )
    agat_isoform        ( gff_isoform.out.filtered_gtf, params.prefix )
    diamond_isoform     ( protein_isoform.out.protein, params.refseq_db, params.prefix )
    busco_isoform       ( protein_isoform.out.protein, params.busco_lineage, params.prefix )
    }
    else{
    }
}
